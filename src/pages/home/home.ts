import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { ServiceHttp } from '../../providers/servicehttp';
import { UrlsConf } from '../../providers/urls.conf';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  createdCode;
  private users:any = null ;
  constructor(public navCtrl: NavController, private servicehttp:ServiceHttp, private urlsConf: UrlsConf, public events: Events,
    public loadingCtrl: LoadingController) {
    let loader = this.loadingCtrl.create({
      content: "Loadding..."
    });
    loader.present();
  	this.servicehttp.get(this.urlsConf.UserProfile).subscribe((response) => {
        if (response.meta == 0) {
        } else {
          this.users = response.data;
        	this.events.publish('user:created', this.users.color);
        }
        loader.dismiss();

        
        this.createdCode = "BEGIN:VCARD\r\n"
          +"VERSION:2.1\r\n"
          +"N:Somebody\r\n"
          +"FN:"+this.users.name+"\r\n"
          +"ORG:"+this.users.passpost+"\r\n"
          +"ADR:"+this.users.country+"\r\n"
          // +"TITLE:"+this.users.country+"\r\n"
          +"TEL;TYPE=cell:"+this.users.phone+"\r\n"
          +"EMAIL;PREF;INTERNET:"+this.users.email+"\r\n"
          +"END:VCARD"
        console.log(response.data);
    })
  }

}
