import { Component, ViewChild} from '@angular/core';
import { Tabs } from 'ionic-angular'
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

import { Events } from 'ionic-angular';
@Component({
	selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild('mytabs') mytabs:Tabs;
  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  private myblue:string = '#95C7DF';  
  constructor(public events: Events) {}


  ngAfterViewInit() {
    this.events.subscribe('user:created', (color) => {
	    if (color) {
	    	this.mytabs._tabbar.nativeElement.style.backgroundColor = color;
	    }
	});
  }
}