import { Component } from '@angular/core';
import { NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { ImagePicker } from '@ionic-native/image-picker';
import { ServiceHttp } from '../../../providers/servicehttp';
import { UrlsConf } from '../../../providers/urls.conf';

@Component({
  selector: 'modal-fillter-page',
  templateUrl: 'modal.html',
})

export class FillterPage {
  imageURI:any;
  imageFileName:any;
  text:string = '';
	constructor(
    public params: NavParams,
    public viewCtrl: ViewController,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    private servicehttp:ServiceHttp, private urlsConf: UrlsConf
  ) {}
	
  public dismiss() {
    this.viewCtrl.dismiss();
  }

  getImage() {
    const options = {
      maximumImagesCount:1,
      width:400,
      height:200,
      quality:100,
      // outputType:1
    }
    this.imagePicker.getPictures(options).then((results) => {
      console.log(results)
      this.imageURI = results[0].substring(7);
      this.uploadImage();
    }, (err) => {
      this.presentToast(err);
    });
  }

  uploadImage() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'source_link',
      fileName: 'ionicfile',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {
        code:"1ba7e7d8115d70d2d00f8c8674fb0a5d"
      }
    }

    fileTransfer.upload(this.imageURI, this.urlsConf.UploadImage, options).then((rp) => {
      let obj = JSON.parse(rp.response);
      this.imageFileName = obj.data.source_link;  
      console.log(this.imageFileName)

      loader.dismiss();
      this.presentToast("Image uploaded successfully");
    }, (err) => {
      console.log(err);
      loader.dismiss();
      this.presentToast(err);
    });
  }

  public post() {
    let loader = this.loadingCtrl.create({
      content: "Posting..."
    });
    loader.present();
    const data = {
      "title": this.text,
      "source_link": this.imageFileName,
      "status": 0,
      "type": "image/jpeg"
    }

    this.servicehttp.post(this.urlsConf.NewPost, data).subscribe((response) => {
      
      if (response.meta == 0) {
        // this.showAlert("Thông báo","Giao dịch chưa thành công!")
      } else {
        this.presentToast(response.message);
      }

      loader.dismiss();
    })

    
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}