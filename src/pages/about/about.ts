import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { ServiceHttp } from '../../providers/servicehttp';
import { UrlsConf } from '../../providers/urls.conf';
import { FillterPage } from './modal/modal';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  private searchText:string = '';
	private datas = [];

  constructor(public navCtrl: NavController, private servicehttp:ServiceHttp, private urlsConf: UrlsConf, private socialSharing: SocialSharing,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
    this.getData();
  }
    
  getData() {
    let loader = this.loadingCtrl.create({
      content: "Loadding..."
    });
    loader.present();
    this.servicehttp.get(this.urlsConf.ListPost).subscribe((response) => {
        // this.loading.dismiss();
        console.log(response)
        if (response.meta == 0) {
          // this.showAlert("Thông báo","Giao dịch chưa thành công!")
        } else {
          this.datas = response.data;
        }
        loader.dismiss();
    })
    
  }
  getItems(ev: any) {
  	const val = ev.target.value;
    this.servicehttp.get(this.urlsConf.BASEURL+'/post/search?title='+val).subscribe((response) => {
        if (response.meta == 0) {
        } else {
          this.datas = response.data;
        }
    })
  }

  shareNow(item) {
    console.log(item)
    this.socialSharing.share(item.title, item.source_link)
  }

  openForm() {
    let fillterModal = this.modalCtrl.create(FillterPage, {});
    fillterModal.onDidDismiss(data => {
      console.log(data);
      // this.getData();
    });

    fillterModal.present();
  }

}
