import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Contacts } from '@ionic-native/contacts';

import { ScrollHideConfig } from '../../directives/scroll-hide';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: undefined };
  headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 44 };
  
  private datas = [];
  constructor(public navCtrl: NavController, private contacts: Contacts) {
    this.contacts.find(['name', 'phoneNumbers'], {filter: "", multiple: true}).then(
      (contact) => {
        this.datas = contact;
      }, (error) => {
        console.log(error);
      }
    );
  }
  
}

