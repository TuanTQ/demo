import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class ServiceHttp {
 
    constructor (
      private http: Http,
    ) {
      this.http = http;
    }
 
    get(url) {
      let headers = new Headers()
        headers.append('Accept', 'application/json'); 
        headers.append('Content-Type', 'application/json');
        headers.append('code', '1ba7e7d8115d70d2d00f8c8674fb0a5d');

      let requestOptions = new RequestOptions({headers: headers});
      return this.http.get(url, requestOptions)
        .map(res => res.json())
        .catch(this.handleError);
    }
 
    post(url, params) {
      let param = JSON.stringify(params);
      let headers = new Headers()
        headers.append('Accept', 'application/json'); 
        headers.append('Content-Type', 'application/json');
        headers.append('code', '1ba7e7d8115d70d2d00f8c8674fb0a5d');

      let requestOptions = new RequestOptions({headers: headers});
      return this.http.post(url, param, requestOptions)
        .map(res => res.json())
        .catch(this.handleError);
    }
 
    handleError(error: Response | any) {
      console.log(error);
    	let errMsg: any;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        // errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        errMsg = {
          status:error.status,
          type:error.type,
        }
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      return Observable.throw(errMsg);
    }

}
