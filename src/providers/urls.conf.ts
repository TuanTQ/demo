import { Injectable } from '@angular/core';

@Injectable()
export class UrlsConf {
	public Root:string = 'http://appsolutely.xzone.work';
	public BASEURL:string = this.Root + '/api';
	public UserProfile:string = this.BASEURL + '/users/profile';
	public UserUpdate:string = this.BASEURL + '/users/update';
	public ListPost:string = this.BASEURL + '/post/list';
	public NewPost:string = this.BASEURL + '/post/store';
	public UploadImage:string = this.BASEURL + '/post/uploadImage';
	public UpdaePost:string = this.BASEURL + '/post/';
	public DeletePost:string = this.BASEURL + '/post/';
}