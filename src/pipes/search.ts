import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  transform(items: any[], terms: string): any[] {
    if(!items) return [];
    if(!terms) return items;
    terms = terms.toLowerCase();
    console.log(terms)
    return items.filter( it => {
    	if (it._objectInstance.name.formatted) {
    		return it._objectInstance.name.formatted.toLowerCase().includes(terms);
    	}
    });
  }
}
