import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { QRCodeModule } from 'angular2-qrcode';
import { Contacts } from '@ionic-native/contacts';
import { SearchPipe } from '../pipes/search';
import { ScrollHideDirective } from '../directives/scroll-hide';

import { UrlsConf } from '../providers/urls.conf';
import { ServiceHttp } from '../providers/servicehttp';
import { FillterPage } from '../pages/about/modal/modal';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { SocialSharing } from '@ionic-native/social-sharing';
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    SearchPipe,
    HomePage,
    TabsPage,
    FillterPage,
    ScrollHideDirective
  ],
  imports: [
    BrowserModule,
    QRCodeModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    FillterPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    Contacts,
    SplashScreen,
    ServiceHttp,
    UrlsConf,
    FileTransfer,
    FileTransferObject,
    File,
    ImagePicker,
    SocialSharing,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
